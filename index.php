<?php
    include "books.php";
    include "db.php";
?>
<html>
    <head>
        <title>Ex4</title>
    </head>
    <body>
        <p>
        <?php
        $db = new DB('localhost','intro','root','');
        $dbc = $db->connect();
        $query = new Query($dbc);
        $q = "SELECT B.title , U.name
              FROM users U 
              JOIN books B ON U.user_id = B.book_id";
        $result = $query->query($q);
        echo '<br>';
        if($result->num_rows > 0){
            echo '<table>';
            echo '<tr><th>Book Title</th><th>User Name</th></tr>';
            while($row = $result->fetch_assoc()){
                echo '<tr>';
                echo '<td>'.$row['title'].'</td><td>'.$row['name'].'</td>';
                echo '</tr>';
            }
            echo '</table>';
        } else{
            echo "Sorry no results";
        }

        ?>
        </p>
    </body>
</html>
